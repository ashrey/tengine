<?php
include 'lib/Parser.php';
include 'lib/Compiler.php';
include 'lib/Node.php';
include 'lib/Token.php';
include 'lib/Branch.php';
include 'lib/Engine.php';

$eng = new Engine(__DIR__.'/tpl', __DIR__.'/cache');
$eng->render('index');
