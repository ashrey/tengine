<?php
define('T_BLOCK', -1);
define('T_ENDBLOCK', -2);
define('T_PIPE', -3);
define('T_COMMA', -4);
define('T_IN', -5);
define('T_INDEX_OPEN', -6);
define('T_INDEX_CLOSE', -7);
define('T_PROP_DOT', -8);
define('T_COLON', -9);

class Token{

	protected $type;
	protected $value;
	static protected $keyword = [
		'block'    => T_BLOCK,
		'endblock' => T_ENDBLOCK,
        'in'       => T_IN,
	];

    static protected $symbol = [
        '|'  => T_PIPE,
        '['  => T_INDEX_OPEN,
        ']'  => T_INDEX_CLOSE,
        ','  => T_COMMA,
        '.'  => T_PROP_DOT,
        ':'  => T_COLON,
    ];

	function __construct($info){
        $this->type  = $info[0];
        $this->value = isset($info[1])?$info[1]:null;
		if(T_STRING == $this->type && isset(self::$keyword[$this->value])){
			$this->type = self::$keyword[$this->value];
		}elseif(is_string($this->type) && isset(self::$symbol[$this->type])){
            $this->value = $this->type;
            $this->type  = self::$symbol[$this->type];
        }elseif (T_CONSTANT_ENCAPSED_STRING == $this->type) {
            $this->value = trim($this->value, '\'"');
        }
	}

	function name(){
		return token_name($this->type);
	}

	function is($type){
        $args = is_array($type) ? $type : func_get_args();
		return in_array($this->type, $args);
	}

	function __invoke(){
		return (bool) $this->type;
	}

	function __get($name){
		return isset($this->$name)?$this->$name:NULL;
	}
}
