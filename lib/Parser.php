<?php
class Parser{
    /**
     * Array for process {} block
     */
	static protected $block  = [
		T_MOD_EQUAL => 'proc_exec', //process
		T_AND_EQUAL => 'proc_interpolation', //interpolation
		T_COMMENT   => 'proc_comm'     //comment
	];

    /**
     * Array fo proccess block
     */
	static protected $proc   =  [
		T_IF       => 'proc_if',
        T_ELSE     => 'proc_else',
		T_ENDIF    => 'proc_endif',
		T_EXTENDS  => 'proc_extend',
		T_STRING   => 'proc_string',
		T_BLOCK    => 'proc_block',
		T_ENDBLOCK => 'proc_endblock',
        T_FOR      => 'proc_for',
        T_ENDFOR   => 'proc_endfor',
        T_INCLUDE  => 'proc_include'
	];

    /**
     * Current file path
     * @var string
     */
	protected $file    = '';

    /**
     * Current line
     */
	protected $line    = 0;

    /**
     * Tokens tree
     * @var array
     */
	protected $tree    = array();

    /**
     * Stack of block code
     * @var array
     */
	protected $last    = array();

    /**
     * Template parent
     * @var Node
     */
	protected $extend;

    /**
     * Store all block on the file,
     * '_main_' is default block
     * $sblock is a FILO stack
     * @var array
     */
	protected $sblock = array('_main_');

    /**
     * Current node
     * @var Node
     */
    protected $node;

    /**
     * @param $filename File path
     */
	function __construct($filename){
		$this->file = $filename;
	}

    /**
     * Main method for to genrate tree
     * @return Array
     */
    function execute(){
        //open file as array
		$lines = file($this->file);
		foreach ($lines as $n => $line) {
			$this->line  = $n+1;
            //process action block
			$current = preg_replace_callback('/\{([#%]|({))(.+)(?(2)\}|\1)}/U', function($m){
				$init = [
                    '{' => '&=',   //print
                    '%' => '%=',   //command
                    '#' => '//'    //comment
                ];
                //position 1 is the type
				$start =  $init[$m[1]];
				$eval  =  $m[3];
				return "<?php $start $eval ?> ";
			}, $line);
            //tokenizer line
			$tree = token_get_all($current);
            $this->node = new Node($tree, $this->line, $this->file);
			$this->check();
		}
		return $this->tree;
	}

    /**
     * Make a checking of the current node
     * @return void
     */
    function check(){
        /**
         * Callback list
         * @var array
         */
		$cb = [
			T_INLINE_HTML => 'proc_html',
			T_OPEN_TAG    => 'proc_tag',
			T_EXTENDS     => 'proc_extend'
		];
        $node = $this->node;
		while($token = $node->next()) {
			if(isset($cb[$token->type])){
				$func  = $cb[$token->type];
				$this->$func();
			}else{
				die('El procesador se fue a la puta');
			}
		}
	}

    /**
     * Return is the file is a child
     */
    function isChild(){
        return is_object($this->extend);
    }

    /**
     * Add branch to the tree
     * @param string $name  type of branch (HTML, etc...)
     * @param string $value value for the tree
     */
	function add($name, $value){
		$current = $this->sblock[0];
        //get the last type of block
		$last    = isset($this->tree[$current]) ? end($this->tree[$current]):null;
        //verification for the last branch
		if($name == 'html' && is_object($last) && $last->isHTML()){
			$last->append($value);
		}else{
            //add the branch on the current block
			$this->tree[$current][] = new Branch($name, $value);
		}
	}

    /**
     * Declare a opening of block
     * @param Integer $token Number token
     * @return void
     */
	function open($token){
		array_unshift($this->last, $token);
	}

    /**
     * Declare closing of block, additionally it checks if the stack is OK
     * @param Integer $token Number token
     * @return void
     */
	function close($token){
		if($this->last[0] != $token)
			throw new Exception("Syntaxys error on line {$this->line}");
		array_shift($this->last);
	}

	function proc_string(){
		$token = $this->node->current();
        do{
            $this->node->next(T_STRING, T_CONSTANT_ENCAPSED_STRING, T_DNUMBER, T_LNUMBER);
        }while($this->node->isNext(T_COMMA) && $this->node->next(T_COMMA));
		$this->node->next(T_CLOSE_TAG);
	}

	function proc_block(){
		$this->open(T_BLOCK);
		$token = $this->node->next();
		$this->add('block', $token->value);
		array_unshift($this->sblock, $token->value);
		$this->node->next(T_CLOSE_TAG);
	}

	function proc_endblock(){
		$this->close(T_BLOCK);
		$this->node->next(T_CLOSE_TAG);
		array_shift($this->sblock);
	}

    /**
     * Process a action tag
     * @return void
     */
	function proc_tag(){
		$token = $this->node->next(T_MOD_EQUAL, T_AND_EQUAL, T_COMMENT);
		$func  = static::$block[$token->type];
		$this->$func();
	}

    /**
     * Process a execution like If, for, etc...
     * @return void
     */
    function proc_exec(){
		$token = $this->node->next();
		$func  = static::$proc[$token->type];
		$this->$func();
	}

    /**
     * Process a set parent
     * @return void
     */
	function proc_extend(){
		$this->extend = $this->node->next(T_CONSTANT_ENCAPSED_STRING, T_STRING);
		$this->node->next(T_CLOSE_TAG);
	}

    /**
     * Process interpolation
     * @return void
     */
	function proc_interpolation(){
		$token = $this->node->next(T_STRING, T_CONSTANT_ENCAPSED_STRING);
        if($this->node->isNext(T_INDEX_OPEN, T_PROP_DOT)){
            $this->proc_var($token);
        }else{
            $this->add('inter', $token->value);
        }
		//has filter TODO
		$token =  $this->node->next(T_CLOSE_TAG, T_PIPE, T_INDEX_OPEN);
        if($token->is(T_PIPE))
            $this->proc_pipe();
	}

    function proc_pipe(){
        do{
            $filter = $this->node->next(T_STRING);
            if($this->node->isNext(T_COLON)){
                do{
                    $this->node->next(T_COLON, T_COMMA);
                    $this->node->next(T_STRING, T_CONSTANT_ENCAPSED_STRING, T_DNUMBER, T_LNUMBER);
                }while($this->node->isNext(T_COMMA));
            }
            $last   = $this->node->next(T_CLOSE_TAG, T_PIPE);
        }while ($last->is(T_PIPE));
    }

    protected function proc_var($token){
        $name  = $token->value;
        $token = $this->node->next(T_INDEX_OPEN, T_PROP_DOT);
        if($token->is(T_INDEX_OPEN)){
            $this->node->next(T_STRING, T_CONSTANT_ENCAPSED_STRING);
            $this->node->next(T_INDEX_CLOSE);
        }else{

        }
    }

    /**
     * Process if
     * @return void
     */
	function proc_if(){
		$this->open(T_IF);
		while(($token = $this->node->next()) && !$token->is(T_CLOSE_TAG)){
			//var_dump($token);
		}
	}

    function proc_else(){
        $this->node->next(T_CLOSE_TAG);
    }

    /**
     * Process end if
     * @return void
     */
	function proc_endif(){
		$this->close(T_IF);
		$this->node->next(T_CLOSE_TAG);
	}

	function proc_comm(){
		$token = $this->node->current();
		$this->add('comm', $token->value);
		$this->node->next(T_CLOSE_TAG);
	}

	function proc_expr($node){

	}

    /**
     * Process a HTML Block
     * @return void
     */
	protected function proc_html(){
		$token = $this->node->current();
		$this->add('html', $token->value);
	}

    function proc_for(){
        $node  = $this->node;
        $this->open(T_FOR);
        $idem = $node->next(T_STRING);
        $elem = $node->next(T_COMMA, T_IN);
        if($elem->is(T_COMMA)){
            $node->next(T_STRING);
            $node->next(T_IN);
        }
        $node->next(T_STRING);
        $node->next(T_CLOSE_TAG);
    }

    function proc_endfor(){
		$this->close(T_FOR);
		$this->node->next(T_CLOSE_TAG);
	}

    function proc_include(){
        $this->proc_string();
    }



    function __get($name){
        return $this->$name;
    }
}
