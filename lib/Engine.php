<?php
class Engine{
	protected $dir;
	protected $cache;

	function __construct($dir, $cache){
		$this->dir   = $dir;
		$this->cache = $cache;
	}

	function render($name, $data=[]){
        $data     = [];
        /*Resolver extends*/
        do{
            $file     = realpath("{$this->dir}/$name.phtml");
            if(!$file)
                throw new RuntimeException("Can't find '$name'", 404);
    		$parse    = new Parser($file);
            $name     = $this->name($file);
    		$compiler = new Compiler($parse, $name);
    		$compiler->init();
    		file_put_contents("cache/{$name}.php", $compiler->getOutput());
            $name     = $parse->extend ? $parse->extend->value: false;
        }while ($name);
	}

    static function name($str){
        return 'Tpl_'.sha1($str);
    }

}
