<?php
class Branch{
	protected $name;
	protected $value;

	function __construct($stack, $line){
		$this->name  = $stack;
		$this->value = $line;
	}

	function append($str){
		$this->value .= $str;
	}

	function __toString(){
		$func = $this->name;
		return $this->$func();
	}

	function isHTML(){
		return $this->name == 'html';
	}

	function html(){
		return "?>{$this->value}<?php ";
	}

	function block(){
		return "self::$this->value();";
	}
	function inter(){
		return "echo \${$this->value};";
	}

	function comm() {
		return '';
	}
}

