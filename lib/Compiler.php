<?php
class Compiler{
	protected $tree;
	protected $output;
    protected $parser;

	function __construct(Parser $parser, $name){
        $this->parser = $parser;
		$this->tree   = $parser->execute();
        $this->name   = $name;
	}

	function header(){
		$this->output  = "<?php\nclass $this->name";
		$this->output .= $this->parser->extend ? 'extend '. Engine::name($this->parser->extend->value):'';
		$this->output .= "{\n";
	}

	function init(){
		$this->header();
		foreach ($this->tree as $key => $value) {
			$this->output .= "static function $key(){\n";
				foreach ($value as $key) {
					$this->output .= $key;
				}
			$this->output .= "}\n";
		}
		$this->output .= '}';
		return $this->output;
	}

	function html(){

	}

    function getOutput(){
        return $this->output;
    }

}
