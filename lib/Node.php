<?php
class Node{
    /**
     * Array of token
     * @var array
     */
	protected $stack = array();

	protected $pos   = -1;
	protected $line  = 0;
    protected $file ;

	function __construct($stack, $line, $file){
		$this->stack = $stack;
		$this->line  = $line;
        $this->file  = $file;
	}

    /**
     * Get a current Token
     * @return Token
     */
	function current(){
		return isset($this->stack[$this->pos]) ?
            new Token($this->stack[$this->pos]) :
            null;
	}

	function next($args=null){
        $args = is_array($args) ? $args : func_get_args();
		$this->pos++;
		$token = $this->current();
		if(is_null($token)) return null;
		if($token->type && $token->is(T_WHITESPACE)){
			return $this->next($args);
		}
		if(!empty($args) && !$token->is($args))
			throw new RuntimeException("Syntaxys error on $this->file line $this->line ".implode($args));
		return $token;
	}

	function isNext($args){
        $args = is_array($args) ? $args : func_get_args();
		$val = $this->next();
		$this->pos--;
		return $val->is($args);
	}
}
